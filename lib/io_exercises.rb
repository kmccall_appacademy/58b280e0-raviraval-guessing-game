# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  comp_guess = computer_guess
  guess_counter = 0
  user_guess = 0
  until user_guess == comp_guess
    puts "guess a number"
    user_guess = gets.chomp.to_i

    if user_guess > computer_guess
      print "#{user_guess}"
      puts "too high"
    else
      print "#{user_guess}"
      puts "too low"
    end

    guess_counter += 1
  end
  puts "Great job, you won!"
  print "The number was #{computer_guess}."
  puts "It took you #{guess_counter} tries to get the number."
end

def computer_guess
  (rand * 100).ceil
end

#* reads that file * shuffles the lines * saves it to the file "{input_name}-shuffled.txt".
def file_shuffler
  puts "give me a file name"
  file_name = gets.chomp
  content = File.readlines(file_name)
  f = File.open("#{file_name}-shuffled.txt", "w") do |f|
    f.puts content.shuffle
  end
end
